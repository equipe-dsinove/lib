ARGS = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

setup:
	make docker-build
	make logs

docker-build:
	docker-compose -f docker/docker-compose.yml up --build -d

logs:
	docker logs service-lib -f

composer-install:
	docker exec service-lib composer install

composer-update:
	docker exec service-lib composer update

composer-test:
	docker exec service-lib composer test

composer-check-style:
	docker exec service-lib composer check-style

composer-cp-detector:
	docker exec service-lib composer cp-detector

composer-diagnosis:
	docker exec service-lib composer diagnosis

composer-analyze:
	docker exec service-lib composer analyze

composer-lint:
	docker exec service-lib composer lint

composer-qa:
	make composer-test
	make composer-check-style
	make composer-cp-detector
	make composer-diagnosis
	make composer-analyze
	make composer-lint

ifndef VERBOSE
.SILENT:
endif

.PHONY:
