<?php

return [
    'service' => [
        'user' => env('APP_URL_USER', 'http://retro-express:8080'),
        'template' => env('APP_URL_TEMPLATE', 'http://retro-express:8080'),
        'project' => env('APP_URL_PROJECT', 'http://retro-express:8080'),
        'order' => env('APP_URL_ORDER', 'http://retro-express:8080'),
        'midia' => env('APP_URL_MIDIA', 'http://retro-express:8080'),
        'order_status' => env('APP_URL_ORDER_STATUS', 'http://retro-express:8080'),
        'render' => env('APP_URL_RENDER', 'http://retro-express-render:8080'),
        'job' => env('APP_URL_JOB', 'http://retro-express-render:8080')
    ],
    'site_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NzgxNjM2MjcsImV4cCI6MTYwOTY5OTYyNywibmJmIjoxNTc4MTYzNjI3LCJqdGkiOiJsY3FaT1o4Uk9qc3N1QzloIiwic3ViIjoyLCJwcnYiOiI0MWRmODgzNGYxYjk4ZjcwZWZhNjBhYWVkZWY0MjM0MTM3MDA2OTBjIiwiZGF0YSI6eyJpZCI6Miwicm9sZXMiOiJbXCJzaXRlXCJdIn19.6LrTMd7IJwrCjsMsDUB7vV3lb3tswghzko7O7jz2RgQ'
];
