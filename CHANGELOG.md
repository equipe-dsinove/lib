# Changelog

## [0.1.0](https://bitbucket.org/equipe-dsinove/model-lib/src/0.1.0/) - 2019-12-28

Removido comandos de criação de api\
Melhorias

## [0.0.1](https://bitbucket.org/equipe-dsinove/model-lib/src/0.0.1/) - 2019-12-16

Lib Composer.

## Desenvolvido e mantido por:
DS Inove Soluções – [david@dsinove.com.br](mailto:david@dsinove.com.br)\
https://bitbucket.org/equipe-dsinove/modelo-service-gateway
