<?php

namespace Sm\Providers;

use Illuminate\Support\ServiceProvider;

class SmServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->configure('microservice');

        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/microservice.php'), 'microservice');
    }
}
