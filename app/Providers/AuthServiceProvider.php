<?php

namespace Sm\Providers;

use Illuminate\Auth\RequestGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Sm\Contracts\Entity as EntityContract;
use Sm\Entities\User;
use Sm\Http\HttpModels\HttpUserModel;
use Sm\Policies\AbstractPolicyApi;
use Sm\Repositories\UserRepository;
use Tymon\JWTAuth\Payload;

class AuthServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerRequestGuard();
    }

    public function boot()
    {
        $this->registerPolicy();
    }

    private function registerPolicy()
    {
        $abstractPolicyApi = new class() extends AbstractPolicyApi
        {
            public static function getKey(): ?string
            {
                return null;
            }

            public static function abilityName(string $ability): string
            {
                return $ability;
            }
        };

        Gate::after(function ($user, $ability) use ($abstractPolicyApi) {
            return $abstractPolicyApi->after($user, $ability);
        });
    }

    private function registerRequestGuard()
    {
        $this->app['auth']->extend('token', function () {
            $callback = [$this, 'tokenAuth'];
            return new RequestGuard($callback, app('request'));
        });
    }

    /**
     * @return User|EntityContract
     */
    private function findUserById(int $id, string $token = null): ?User
    {
        $model = (new HttpUserModel())->setTokenBearer($token);
        return (new UserRepository($model))->apiFind($id);
    }

    public function tokenAuth(Request $request)
    {
        $jwt = $this->app->make(\Tymon\JWTAuth\JWTAuth::class);
        $jwt->setRequest($request);

        $payload = null;

        if ($jwt->parseToken()) {
            $payload = $jwt->setToken($request->bearerToken())->getPayload();
        }

        if ($payload instanceof Payload) {
            return $this->findUserById($payload->get('data')->id, $request->bearerToken());
        }

        return null;
    }
}
