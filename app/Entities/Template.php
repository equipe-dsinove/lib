<?php

namespace Sm\Entities;

use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;

class Template implements EntityContract
{
    use ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    private $id;
    private $userId;
    private $name;
    private $video;
    private $options = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;
        return $this;
    }

    /**
     * @param TemplateOption[] $options
     * @return $this
     */
    public function setOptions(array $options): self
    {
        collect($options)->each(function (TemplateOption $option) {
            $this->addOption($option);
        });

        return $this;
    }

    /**
     * @return TemplateOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    public function addOption(TemplateOption $option): self
    {
        array_push($this->options, $option);
        return $this;
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        $data['options'] = collect($this->getOptions())->map(function (TemplateOption $option) {
            return $option->toArray(true);
        });

        unset($data['template_id']);

        return array_merge($data, $this->humanizeTimestamps());
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray();

        if (isset($data['options'])) {
            unset($data['options']);
        }

        return $data;
    }
}
