<?php

namespace Sm\Entities;

use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;

class Project implements EntityContract
{
    use ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    private $id;
    private $userId;
    private $templateOption;
    private $name;

    private $assets = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getTemplateOption(): ?TemplateOption
    {
        return $this->templateOption;
    }

    public function setTemplateOption(?TemplateOption $templateOption): self
    {
        $this->templateOption = $templateOption;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getAssets(): array
    {
        return $this->assets;
    }

    public function setAssets(array $assets): self
    {
        $this->assets = $assets;
        return $this;
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        return array_merge($data, $this->humanizeTimestamps());
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray($update);

        $data['template_option_id'] = $this->getTemplateOption()->getId();

        $data['assets'] = json_encode($data['assets']);

        if ($data['template_option']) {
            unset($data['template_option']);
        }

        return $data;
    }
}
