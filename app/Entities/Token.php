<?php

namespace Sm\Entities;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Contracts\Support\Jsonable as JsonableContract;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;

class Token implements ArrayableContract, JsonableContract
{
    use ToJson;

    private $accessToken;
    private $type;
    private $expiresIn;

    public function __construct(string $accessToken, string $type, int $expiresIn)
    {
        $this->accessToken = $accessToken;
        $this->type = $type;
        $this->expiresIn = $expiresIn;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }
}
