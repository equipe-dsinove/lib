<?php

namespace Sm\Entities;

use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;
use \InvalidArgumentException;

class OrderStatus implements EntityContract
{
    use ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    //Aguardando pagamento
    const AWAITING_PAYMENT_ID = 0;
    const AWAITING_PAYMENT_TEXT = 'Awaiting payment';
    // Pagamento confirmado
    const PAYMENT_CONFIRMED_ID = 1;
    const PAYMENT_CONFIRMED_TEXT = 'Payment confirmed';

    //Prazo de pagamento expirado
    const EXPIRED_DEADLINE_ID = 2;
    const EXPIRED_DEADLINE_TEXT = 'Expired deadline';

    //Em fila de processamento
    const QUEUED_PROCESSING_ID = 3;
    const QUEUED_PROCESSING_TEXT = 'Queued processing';

    //Processando
    const PROCESSING_ID = 4;
    const PROCESSING_TEXT = 'Processing';

    //Erro de processamento
    const PROCESSING_ERROR_ID = 5;
    const PROCESSING_ERROR_TEXT = 'Processing error';

    //Disponivel
    const AVAILABLE_ID = 6;
    const AVAILABLE_TEXT = 'Available';

    //Estornado
    const REVERSED_ID = 7;
    const REVERSED_TEXT = 'Reversed';

    const STATUS = [
        self::AWAITING_PAYMENT_ID => self::AWAITING_PAYMENT_TEXT,
        self::PAYMENT_CONFIRMED_ID => self::PAYMENT_CONFIRMED_TEXT,
        self::QUEUED_PROCESSING_ID => self::QUEUED_PROCESSING_TEXT,
        self::PROCESSING_ID => self::PROCESSING_TEXT,
        self::PROCESSING_ERROR_ID => self::PROCESSING_ERROR_TEXT,
        self::AVAILABLE_ID => self::AVAILABLE_TEXT,
        self::REVERSED_ID => self::REVERSED_TEXT
    ];

    private $id;
    private $orderId;
    private $status;
    private $detail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $status = empty($status) ? self::AWAITING_PAYMENT_ID : $status;

        if (!in_array($status, array_keys(self::STATUS))) {
            throw new InvalidArgumentException(__('Invalid status'));
        }

        $this->status = $status;
        return $this;
    }

    public function getStatusText(): ?string
    {
        if (null !== $this->getStatus()) {
            return self::STATUS[$this->getStatus()];
        }

        return null;
    }

    public static function listStatus(): array
    {
        return self::STATUS;
    }

    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    public function setOrderId(?int $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(?string $detail): self
    {
        $this->detail = $detail;
        return $this;
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        return array_merge($data, $this->humanizeTimestamps());
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray($update);

        unset($data['status_text']);

        return $data;
    }
}
