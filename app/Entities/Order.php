<?php

namespace Sm\Entities;

use Carbon\CarbonInterface as CarbonContract;
use Sm\Contracts\Entities\Timestampsable as TimestampsableContract;
use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;

class Order implements EntityContract
{
    use ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    private $id;
    private $userId;
    private $projectId;
    private $jobId;
    private $transaction;
    private $amount;
    private $dueDate;
    private $status = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getProjectId(): ?int
    {
        return $this->projectId;
    }

    public function setProjectId(?int $projectId): self
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getTransaction(): ?string
    {
        return $this->transaction;
    }

    public function setJobId(?int $jobId): self
    {
        $this->jobId = $jobId;
        return $this;
    }

    public function getJobId(): ?int
    {
        return $this->jobId;
    }

    public function setTransaction(?string $transaction): self
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function getDueDate(): ?CarbonContract
    {
        return $this->dueDate;
    }

    public function setDueDate(?CarbonContract $dueDate): self
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    public function getStatus(): array
    {
        return $this->status;
    }

    public function setStatus(array $status): self
    {
        foreach ($status as $st) {
            $this->addStatus($st);
        }
        return $this;
    }

    public function addStatus(OrderStatus $status): self
    {
        array_push($this->status, $status);
        return $this;
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        $data['due_date'] = $this->getDueDate()
            ? $this->getDueDate()->format(TimestampsableContract::FORMAT_DATE_TIME)
            : null;

        $data['status'] = array_map(function (OrderStatus $status) {
            return $status->toArray();
        }, $this->getStatus());

        return array_merge($data, $this->humanizeTimestamps());
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray($update);

        unset($data['status']);

        return $data;
    }
}
