<?php

namespace Sm\Entities;

use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TemplateOption implements EntityContract
{
    const COMPOSITION_MAIN = 'main';
    const COMPOSITION_SD = 'sd';
    const COMPOSITION_HD = 'hd';
    const COMPOSITION_4K = '4k';

    const COMPOSITIONS = [
        self::COMPOSITION_MAIN,
        self::COMPOSITION_SD,
        self::COMPOSITION_HD,
        self::COMPOSITION_4K
    ];

    use ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    private $id;
    private $src;
    private $composition;
    private $length;
    private $amount;
    private $assets = [];
    private $templateId;
    private $file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSrc(): ?string
    {
        return $this->src;
    }

    public function setSrc(?string $src): self
    {
        $this->src = $src;
        return $this;
    }

    public function getComposition(): ?string
    {
        return $this->composition;
    }

    public function setComposition(?string $composition): self
    {
        if (!in_array($composition, self::COMPOSITIONS)) {
            throw new \InvalidArgumentException(__('Composition invalid'));
        }

        $this->composition = $composition;
        return $this;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getTemplateId(): ?int
    {
        return $this->templateId;
    }

    public function setTemplateId(?int $templateId): self
    {
        $this->templateId = $templateId;
        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(?int $length): self
    {
        $this->length = $length;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function getAssets(): array
    {
        return $this->assets;
    }

    public function setAssets(array $assets): self
    {
        $this->assets = $assets;
        return $this;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    public function setFile(?UploadedFile $file): self
    {
        $this->file = $file;
        return $this;
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        unset($data['file']);

        return $data;
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray($update);

        $data['assets'] = json_encode($data['assets']);

        unset($data['file']);

        return $data;
    }
}
