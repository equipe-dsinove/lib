<?php

namespace Sm\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToArray;
use Sm\Traits\ToJson;

class User implements EntityContract, AuthenticatableContract
{

    const ROLE_SUPER_ADMIN = 'super-admin';
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_SITE = 'site';

    use Authenticatable, ToArray, ToJson, Timestamps {
        ToArray::toArray as toArrayTrait;
    }

    private $id;
    private $useId;
    private $name;
    private $phone;
    private $email;
    private $password;
    private $passwordRaw;
    private $roles = [];
    protected $remember_token;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getUseId(): ?int
    {
        return $this->useId ? $this->useId : $this->getId();
    }

    public function setUseId(?int $useId): self
    {
        $this->useId = $useId;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPasswordHash(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function createPasswordHash($password): self
    {
        $this->password = EntityFactory::createPasswordHash($password);
        return $this;
    }

    public function setPasswordRaw($passwordRaw): self
    {
        $this->passwordRaw = $passwordRaw;
        return $this;
    }

    public function getPasswordRaw()
    {
        return $this->passwordRaw;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function isSuperAdmin(): bool
    {
        return in_array(self::ROLE_SUPER_ADMIN, $this->getRoles());
    }

    public function isAdmin(): bool
    {
        return $this->isSuperAdmin() ? true : in_array(self::ROLE_ADMIN, $this->getRoles());
    }

    public function isUser(): bool
    {
        return $this->isAdmin() ? true : in_array(self::ROLE_USER, $this->getRoles());
    }

    public function isSite(): bool
    {
        return $this->isUser() ? true : in_array(self::ROLE_SITE, $this->getRoles());
    }

    public function getKeyName()
    {
        return 'id';
    }

    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = $this->toArrayTrait($excludeEmpty);

        unset(
            $data['key_name'],
            $data['auth_identifier_name'],
            $data['auth_identifier'],
            $data['remember_token_name'],
            $data['auth_password'],
            $data['remember_token'],
            $data['use_id']
        );

        $data['is_super_admin'] = $this->isSuperAdmin();
        $data['is_admin'] = $this->isAdmin();
        $data['is_user'] = $this->isUser();
        $data['is_site'] = $this->isSite();

        return array_merge($data, $this->humanizeTimestamps());
    }

    public function toArrayDb(bool $update = null): array
    {
        $data = $this->toArray($update);

        $data['roles'] = json_encode($data['roles']);

        unset($data['is_super_admin'], $data['is_admin'], $data['is_user'], $data['is_site']);

        return $data;
    }
}
