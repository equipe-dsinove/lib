<?php

namespace Sm\Entities;

use Sm\Contracts\Entity as EntityContract;
use Sm\Traits\Timestamps;
use Sm\Traits\ToJson;

class Job implements EntityContract
{
    use ToJson, Timestamps;

    private $id;
    private $orderId;
    private $project;

    public function __construct(int $orderId = null, Project $project = null)
    {
        $this->orderId = $orderId;
        $this->project = $project;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    public function setOrderId(?int $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;
        return $this;
    }

    public function toArrayDb(bool $update = null): array
    {
        return $this->toArray();
    }
}
