<?php

namespace Sm\Entities;
;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Sm\Contracts\Entities\Timestampsable as TimestampsableContract;
use Sm\Contracts\Entity as EntityContract;
use Sm\Exceptions\NotFoundEntityException;

class EntityFactory
{
    private static function setTimestamps(EntityContract &$entity, Collection $data): void
    {
        $createdAt = $data->get(Model::CREATED_AT, $data->get('createdAt'));
        $updatedAt = $data->get(Model::UPDATED_AT, $data->get('updatedAt'));

        $entity->setCreatedAt($createdAt ? Carbon::make($createdAt) : null)
            ->setUpdatedAt($updatedAt ? Carbon::make($updatedAt) : null);
    }

    public static function createUser(Collection $data, EntityContract $entity = null): User
    {
        $entity = $entity instanceof User ? $entity : new User();
        $data = collect(array_merge($entity->toArray(), $data->toArray()));
        $roles = $data->get('roles') ?: [];

        if (is_string($roles)) {
            $roles = json_decode($roles, true);
        }

        if ($entity instanceof TimestampsableContract) {
            self::setTimestamps($entity, $data);
        }

        $entity
            ->setRoles($roles)
            ->setId($data->get('id'))
            ->setName($data->get('name'))
            ->setPhone($data->get('phone'))
            ->setEmail($data->get('email'))
            ->setPasswordRaw($data->get('password_raw'))
            ->setPasswordHash($data->get('password'));

        if ($data->get('password_raw')) {
            $entity->createPasswordHash($data->get('password_raw'));
        }

        return $entity;
    }

    public static function createPasswordHash($password): string
    {
        return app('hash')->make($password);
    }

    public static function createTemplate(Collection $data, EntityContract $entity = null): Template
    {
        $entity = $entity instanceof Template ? $entity : new Template();

        $options = $data->get('options') instanceof Arrayable
            ? $data->get('options')->toArray()
            : $data->get('options', []);

        $data = collect(array_merge($entity->toArray(), $data->toArray()));
        $data->put('options', $options);

        self::setTimestamps($entity, $data);

        $options = array_map(function ($item) {
            return self::createTemplateOption(collect($item));
        }, $data->get('options'));

        return $entity
            ->setId($data->get('id'))
            ->setUserId($data->get('user_id'))
            ->setName($data->get('name'))
            ->setVideo($data->get('video'))
            ->setOptions($options);
    }

    public static function createTemplateOption(Collection $data, EntityContract $entity = null): TemplateOption
    {
        $entity = $entity instanceof TemplateOption ? $entity : new TemplateOption();
        $data = collect(array_merge($entity->toArray(), $data->toArray()));

        self::setTimestamps($entity, $data);

        $assets = $data->get('assets', []);

        if (is_string($assets)) {
            $assets = json_decode($assets);
        }

        return $entity
            ->setId($data->get('id'))
            ->setTemplateId($data->get('template_id'))
            ->setAmount($data->get('amount'))
            ->setLength($data->get('length'))
            ->setAssets($assets)
            ->setFile($data->get('file'))
            ->setSrc($data->get('src'))
            ->setComposition($data->get('composition'));
    }

    public static function createProject(Collection $data, EntityContract $entity = null): Project
    {
        $entity = $entity instanceof Project ? $entity : new Project();

        $templateOptionData = $data->get('template_option') instanceof Arrayable
            ? $data->get('template_option')->toArray()
            : $data->get('template_option', []);

        $data = collect(array_merge($entity->toArray(), $data->toArray()));
        $data->put('template_option', $templateOptionData);

        self::setTimestamps($entity, $data);

        $assets = $data->get('assets', []);

        if (is_string($assets)) {
            $assets = json_decode($assets);
        }

        return $entity
            ->setId($data->get('id'))
            ->setUserId($data->get('user_id'))
            ->setName($data->get('name'))
            ->setAssets($assets)
            ->setTemplateOption(self::createTemplateOption(collect($data->get('template_option'))));
    }

    public static function createOrder(Collection $data, EntityContract $entity = null): Order
    {
        $entity = $entity instanceof Order ? $entity : new Order();

        $status = array_map(function ($item) {
            return self::createOrderStatus(collect(collect($item)));
        }, $data->get('status', []));


        $data = collect(array_merge($entity->toArray(), $data->toArray()));

        self::setTimestamps($entity, $data);

        return $entity
            ->setId($data->get('id'))
            ->setUserId($data->get('user_id'))
            ->setProjectId($data->get('project_id'))
            ->setJobId($data->get('job_id'))
            ->setAmount($data->get('amount'))
            ->setDueDate($data->get('due_date') ? Carbon::make($data->get('due_date')) : null)
            ->setTransaction($data->get('transaction'))
            ->setStatus($status);
    }

    public static function createOrderStatus(Collection $data, EntityContract $entity = null): OrderStatus
    {
        $entity = $entity instanceof OrderStatus ? $entity : new OrderStatus();

        $data = collect(array_merge($entity->toArray(), $data->toArray()));

        self::setTimestamps($entity, $data);

        return $entity
            ->setId($data->get('id'))
            ->setOrderId($data->get('order_id'))
            ->setStatus($data->get('status'))
            ->setDetail($data->get('detail'));
    }

    public static function createJob(Collection $data, EntityContract $entity = null): Job
    {
        $entity = $entity instanceof Job ? $entity : new Job();

        $data = collect(array_merge($entity->toArray(), $data->toArray()));

        $project = self::createProject(collect($data->get('project')));

        self::setTimestamps($entity, $data);

        return $entity->setId($data->get('id'))
            ->setOrderId($data->get('order_id'))
            ->setProject($project);
    }

    public static function createMidia(Collection $data, EntityContract $entity = null): Midia
    {
        $entity = $entity instanceof Midia ? $entity : new Midia();

        $data = collect(array_merge($entity->toArray(), $data->toArray()));

        self::setTimestamps($entity, $data);

        return $entity->setId($data->get('id'))
            ->setUserId($data->get('user_id'))
            ->setPath($data->get('path'))
            ->setExtension($data->get('extension'))
            ->setMimeType($data->get('mime_type'))
            ->setSize($data->get('size'));
    }

    public static function create(string $entityClassName, Collection $data, EntityContract $entity = null)
    {
        if ($entityClassName === User::class) {
            return self::createUser($data, $entity);
        }

        if ($entityClassName === Template::class) {
            return self::createTemplate($data, $entity);
        }

        if ($entityClassName === TemplateOption::class) {
            return self::createTemplateOption($data, $entity);
        }

        if ($entityClassName === Project::class) {
            return self::createProject($data, $entity);
        }

        if ($entityClassName === Order::class) {
            return self::createOrder($data, $entity);
        }

        if ($entityClassName === Job::class) {
            return self::createJob($data, $entity);
        }

        if ($entityClassName === Midia::class) {
            return self::createMidia($data, $entity);
        }

        throw new NotFoundEntityException();
    }
}
