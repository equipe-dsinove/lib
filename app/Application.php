<?php

namespace Sm;

use Laravel\Lumen\Application as LumenApplication;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Application extends LumenApplication
{
    public function __construct($basePath = null)
    {
        parent::__construct($basePath);
        $this->registerProviders();
        $this->registerLogBindings();
    }

    protected function registerProviders()
    {
        $this->register(\Sm\Providers\SmServiceProvider::class);
        $this->register(\Sm\Providers\JWTServiceProvider::class);
        $this->register(\Sm\Providers\AuthServiceProvider::class);
    }

    protected function registerErrorHandling()
    {
        parent::registerErrorHandling();

        $this->singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Sm\Exceptions\Handler::class
        );
    }

    protected function registerLogBindings()
    {
        $this->singleton(\Psr\Log\LoggerInterface::class, function () {
            return new Logger('lumen', $this->getMonologHandler());
        });
    }

    /**
     * @throws \Exception
     */
    protected function getMonologHandler(): array
    {
        $handlers = [];
        $logLevel = config('app.debug', false) ? Logger::DEBUG : Logger::WARNING;
        $handlers[] = (new StreamHandler(env('APP_LOG_PATH', '/tmp/app-error.log'), $logLevel))
            ->setFormatter(new LineFormatter(null, null, true, true));
        return $handlers;
    }

    public function bootstrapRouter()
    {
        parent::bootstrapRouter();

        $this->routeMiddleware([
            'auth' => \Sm\Http\Middleware\Authenticate::class
        ]);
    }
}
