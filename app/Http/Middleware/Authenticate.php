<?php

namespace Sm\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            $response = api_response_error('Unauthorized.', [
                'Token expired or missing.'
            ], Response::HTTP_UNAUTHORIZED);

            return response($response->toArray(), $response->getStatusCode());
        }

        if ($this->auth->user() instanceof Model) {
            $this->auth->setUser($this->auth->user()->entity());
        }

        $this->auth->user()->setRememberToken($request->bearerToken());

        return $next($request);
    }
}
