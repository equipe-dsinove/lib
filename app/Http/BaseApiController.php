<?php

namespace Sm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller;
use Sm\Contracts\ApiResponse as ApiResponseContract;
use Sm\Contracts\Services\ServiceApi as ServiceApiContract;
use Sm\Helpers\ApiResponse;
use Sm\Helpers\ApiResponsePagination;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;

class BaseApiController extends Controller
{
    private $request;
    private $service;

    public function __construct(Request $request, ServiceApiContract $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getService(): ServiceApiContract
    {
        return $this->service;
    }

    public function apiResponse(ApiResponseContract $apiResponse): Response
    {
        return response($apiResponse->toArray(), $apiResponse->getStatusCode());
    }

    public function apiFind(int $id): Response
    {
        $data = $this->getService()->apiFind($id, (new Fields())->createByRequest($this->getRequest()));

        return $this->apiResponse(ApiResponse::create($data));
    }

    public function apiFindBy(): Response
    {
        $data = $this->getService()->apiFindBy(new BuildFilter($this->getRequest()));

        return $this->apiResponse(ApiResponsePagination::create($data));
    }

    public function apiCreate()
    {
        $data = $this->getService()->apiCreate($this->getRequest());

        return $this->apiResponse(ApiResponse::create($data, Response::HTTP_CREATED));
    }

    public function apiUpdate(int $id)
    {
        $data = $this->getService()->apiUpdate($id, $this->getRequest());

        return $this->apiResponse(ApiResponse::create($data, Response::HTTP_ACCEPTED));
    }

    public function apiUpdatePatch(int $id)
    {
        $data = $this->getService()->apiUpdatePatch($id, $this->getRequest());

        return $this->apiResponse(ApiResponse::create($data, Response::HTTP_ACCEPTED));
    }

    public function apiDelete(int $id)
    {
        $data = $this->getService()->apiDelete($id);

        return $this->apiResponse(ApiResponse::create($data, Response::HTTP_NO_CONTENT));
    }
}
