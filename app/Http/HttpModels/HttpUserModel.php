<?php

namespace Sm\Http\HttpModels;

use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Entities\EntityFactory;
use Sm\Entities\User;
use Sm\Helpers\QueryFilters\Fields;

class HttpUserModel extends AbstractHttpModelApi
{
    protected $path = 'user';

    const PATH_GET_ME = '{path}/me';

    public function __construct()
    {
        $this->baseUri = config('microservice.service.user');
    }

    /**
     * @param Fields|null $fields
     * @return ModelApiContract
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function me(?Fields $fields = null): ModelApiContract
    {
        $query = [];

        if ($fields) {
            $query = $fields->toQuery()->all();
        }

        $this->setData($this->http($this->baseUri)->get(strtr(self::PATH_GET_ME, [
            '{path}' => $this->path
        ]), $query)->toArray());

        return $this;
    }

    /**
     * @return EntityContract|User
     */
    public function entity(): EntityContract
    {
        return EntityFactory::createUser(collect($this->toArray()));
    }
}
