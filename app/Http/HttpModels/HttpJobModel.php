<?php

namespace Sm\Http\HttpModels;

use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Entities\EntityFactory;
use Sm\Entities\Job;

class HttpJobModel extends AbstractHttpModelApi
{
    protected $path = 'render/job';

    public function __construct()
    {
        $this->baseUri = config('microservice.service.job');
    }

    public function apiCreate(EntityContract $entity): ModelApiContract
    {
        $data = parent::apiCreate($entity);

        $data->setData(array_merge($data->toArray(), $entity->toArray(true)));

        return $data;
    }

    /**
     * @return EntityContract|Job
     */
    public function entity(): EntityContract
    {
        return EntityFactory::createJob(collect($this->toArray()));
    }
}
