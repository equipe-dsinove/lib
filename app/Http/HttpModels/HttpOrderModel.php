<?php

namespace Sm\Http\HttpModels;

use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Entities\EntityFactory;
use Sm\Contracts\OrderMethods as OrderMethodsContract;
use Sm\Entities\Order;

class HttpOrderModel extends AbstractHttpModelApi implements OrderMethodsContract
{
    protected $path = 'order';

    public function __construct()
    {
        $this->baseUri = config('microservice.service.order');
    }

    public function status(EntityContract &$entity): ModelApiContract
    {
        return app(HttpOrderStatusModel::class)->apiCreate($entity);
    }

    /**
     * @return EntityContract|Order
     */
    public function entity(): EntityContract
    {
        return EntityFactory::createOrder(collect($this->toArray()));
    }
}
