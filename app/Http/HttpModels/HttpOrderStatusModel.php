<?php

namespace Sm\Http\HttpModels;

use Sm\Contracts\Entity as EntityContract;
use Sm\Entities\EntityFactory;
use Sm\Entities\OrderStatus;

class HttpOrderStatusModel extends AbstractHttpModelApi
{
    protected $path = 'order/status';

    public function __construct()
    {
        $this->baseUri = config('microservice.service.order_status');
    }

    /**
     * @return EntityContract|OrderStatus
     */
    public function entity(): EntityContract
    {
        return EntityFactory::createOrderStatus(collect($this->toArray()));
    }
}
