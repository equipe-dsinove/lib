<?php

namespace Sm\Http\HttpModels;

use Sm\Contracts\Entity as EntityContract;
use Sm\Entities\EntityFactory;
use Sm\Entities\Midia;

class HttpMidiaModel extends AbstractHttpModelApi
{
    protected $path = 'midia';

    public function __construct()
    {
        $this->baseUri = config('microservice.service.midia');
    }

    /**
     * @return EntityContract|Midia
     */
    public function entity(): EntityContract
    {
        return EntityFactory::createMidia(collect($this->toArray()));
    }
}
