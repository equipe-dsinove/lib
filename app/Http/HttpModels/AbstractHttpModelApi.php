<?php

namespace Sm\Http\HttpModels;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as PaginatorContract;
use Illuminate\Pagination\LengthAwarePaginator;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Helpers\Pagination\Pagination;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;
use Sm\Services\HttpService;

abstract class AbstractHttpModelApi implements ModelApiContract
{
    const PATH = '/{path}';
    const PATH_PARAM = self::PATH . '/{id}';

    protected $baseUri;
    protected $path;
    private $tokenBearer;
    private $data;

    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function setTokenBearer(?string $tokenBearer): self
    {
        $this->tokenBearer = $tokenBearer;
        return $this;
    }

    protected function http(string $baseUri, array $config = []): HttpService
    {
        $tokenBearer = null;

        if ($this->tokenBearer) {
            $tokenBearer = $this->tokenBearer;
        }

        return HttpService::create($baseUri, $config, $tokenBearer);
    }

    public function getKeyName(): string
    {
        return 'id';
    }

    /**
     * @param int $id
     * @param Fields|null $fields
     * @return ModelApiContract
     * @throws GuzzleException
     */
    public function apiFind(int $id, ?Fields $fields = null): ModelApiContract
    {
        $query = [];
        if ($fields) {
            $query = $fields->toQuery()->all();
        }

        $this->data = $this->http($this->baseUri)->get(strtr(self::PATH_PARAM, [
            '{path}' => $this->path,
            '{id}' => $id
        ]), $query)->toArray();

        return $this;
    }

    /**
     * @param BuildFilter $build
     * @return PaginatorContract
     * @throws GuzzleException
     */
    public function apiFindBy(BuildFilter $build): PaginatorContract
    {
        $data = $this->http($this->baseUri)->get(strtr(self::PATH, [
            '{path}' => $this->path
        ]), $build->toQuery()->all());

        $items = collect($data->getData())->map(function ($data) {
            $model = clone $this;
            $model->setData($data);
            return $model;
        });

        return new LengthAwarePaginator($items, count($items), Pagination::LIMIT_DEFAULT);
    }

    /**
     * @param EntityContract $entity
     * @return ModelApiContract
     * @throws GuzzleException
     */
    public function apiCreate(EntityContract $entity): ModelApiContract
    {
        $this->data = $this->http($this->baseUri)->post(strtr(self::PATH, [
            '{path}' => $this->path
        ]), $entity->toArray(true))->toArray();

        return $this;
    }

    /**
     * @param int $id
     * @param EntityContract $entity
     * @param bool|null $partial
     * @return int
     * @throws GuzzleException
     */
    public function apiUpdate(int $id, EntityContract $entity, bool $partial = null): int
    {

        $path = strtr(self::PATH_PARAM, [
            '{path}' => $this->path,
            '{id}' => $id
        ]);
        $body = $entity->toArray(true);

        if ($partial) {
            $data = $this->http($this->baseUri)->patch($path, $body);
        } else {
            $data = $this->http($this->baseUri)->put($path, $body);
        }

        return (int)$data->isSuccess();
    }

    /**
     * @param int $id
     * @return int
     * @throws GuzzleException
     */
    public function apiDelete(int $id): int
    {
        $data = $this->http($this->baseUri)->delete(strtr(self::PATH_PARAM, [
            '{path}' => $this->path,
            '{id}' => $id
        ]));

        return (int)$data->isSuccess();
    }

    public function toArray(): array
    {
        return $this->data;
    }

    abstract public function entity(): EntityContract;
}
