<?php

namespace Sm\Console;

use Laravel\Lumen\Application;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    public function __construct(Application $app)
    {
        parent::__construct($app);
    }
}
