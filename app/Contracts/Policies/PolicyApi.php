<?php

namespace Sm\Contracts\Policies;

use Sm\Contracts\Entity as ContractEntity;
use Sm\Helpers\Pagination\Pagination;

interface PolicyApi
{
    const API_FIND = 'apiFind';
    const API_FIND_BY = 'apiFindBy';
    const API_CREATE = 'apiCreate';
    const API_UPDATE = 'apiUpdate';
    const API_DELETE = 'apiDelete';

    public static function getKey(): ?string;

    public function apiFind($user, ContractEntity $entity): bool;

    public function apiFindBy($user, Pagination $pagination): bool;

    public function apiCreate($user, ContractEntity $entity): bool;

    public function apiUpdate($user, ContractEntity $entity): bool;

    public function apiDelete($user, ContractEntity $entity): bool;

    public static function abilityName(string $ability): string;

    public static function create(): self;
}
