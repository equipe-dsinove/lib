<?php

namespace Sm\Contracts;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Contracts\Support\Jsonable as JsonableContract;

interface ApiResponse extends ArrayableContract, JsonableContract
{
    public function getData();

    public function getStatusCode(): int;
}
