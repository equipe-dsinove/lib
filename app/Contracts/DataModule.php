<?php

namespace Sm\Contracts;

use Laravel\Lumen\Application;

interface DataModule
{
    public function register(Application $app);
}
