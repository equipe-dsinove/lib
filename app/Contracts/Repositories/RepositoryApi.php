<?php

namespace Sm\Contracts\Repositories;

use Illuminate\Database\Eloquent\Model;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Helpers\Pagination\Pagination;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;

interface RepositoryApi
{
    /**
     * @return Model|ModelApiContract
     */
    public function getModel(): ModelApiContract;

    public function createEntity(?array $data, EntityContract $entity = null): ?EntityContract;

    public function apiFind(int $id, ?Fields $fields = null);

    public function apiFindBy(BuildFilter $build): Pagination;

    public function apiCreate(EntityContract &$entity): bool;

    public function apiUpdate(int $id, EntityContract &$entity, bool $partial = null): bool;

    public function apiDelete(int $id): bool;
}
