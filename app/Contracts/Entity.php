<?php

namespace Sm\Contracts;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Contracts\Support\Jsonable as JsonableContract;
use Sm\Contracts\Entities\Timestampsable as TimestampsableContract;

interface Entity extends ArrayableContract, JsonableContract, TimestampsableContract
{
    public function getId(): ?int;

    public function toArray(?bool $excludeEmpty = null): array;

    public function toArrayDb(bool $update = null): array;
}
