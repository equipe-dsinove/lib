<?php

namespace Sm\Contracts;

use Sm\Contracts\Entity as EntityContract;

interface OrderMethods
{
    public function status(EntityContract &$entity);
}
