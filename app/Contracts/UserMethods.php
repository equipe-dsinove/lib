<?php

namespace Sm\Contracts;

use Sm\Contracts\Entity as EntityContract;
use Sm\Helpers\QueryFilters\Fields;

interface UserMethods
{
    public function me(?Fields $fields = null);
}
