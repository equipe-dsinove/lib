<?php

namespace Sm\Contracts\Services;

use Illuminate\Http\Request;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Policies\PolicyApi as PolicyContract;
use Sm\Contracts\Repositories\RepositoryApi as RepositoryContract;
use Sm\Helpers\Pagination\Pagination;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;

interface ServiceApi
{
    public function __construct(RepositoryContract $repository, PolicyContract $policyApi = null);

    public function getRepository(): RepositoryContract;

    public function getPolicy(): PolicyContract;

    public function apiFind(int $id, Fields $fields): ?EntityContract;

    public function apiFindBy(BuildFilter $build): Pagination;

    public function apiCreate(Request $request): ?EntityContract;

    public function apiUpdate(int $id, Request $request): ?EntityContract;

    public function apiUpdatePatch(int $id, Request $request): ?EntityContract;

    public function apiDelete(int $id): bool;
}
