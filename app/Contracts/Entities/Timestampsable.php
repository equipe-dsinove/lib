<?php

namespace Sm\Contracts\Entities;

use Carbon\CarbonInterface as CarbonContract;

interface Timestampsable
{
    const FORMAT_DATE_TIME = 'Y-m-d H:i:s';

    public function getCreatedAt(): ?CarbonContract;

    public function setCreatedAt(?CarbonContract $createdAt): Timestampsable;

    public function getUpdatedAt(): ?CarbonContract;

    public function setUpdatedAt(?CarbonContract $updatedAt): Timestampsable;

    public function humanizeTimestamps(): array;

}
