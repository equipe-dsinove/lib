<?php

namespace Sm\Contracts\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator as PaginatorContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Sm\Contracts\Entity as EntityContract;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;

interface ModelApi extends ArrayableContract
{
    public function getKeyName(): string;

    /**
     * @param int $id
     * @param Fields|null $fields
     * @return ModelApi|null
     */
    public function apiFind(int $id, ?Fields $fields = null): ?ModelApi;

    public function apiFindBy(BuildFilter $build): PaginatorContract;

    public function apiCreate(EntityContract $entity): ModelApi;

    public function apiUpdate(int $id, EntityContract $entity, bool $partial = null): int;

    public function apiDelete(int $id): int;

    public function entity(): EntityContract;
}
