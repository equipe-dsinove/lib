<?php

namespace Sm\Contracts\Helpers\QueryFilters;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;
use \Sm\Contracts\Helpers\QueryFilters\ToQuery as ToQueryContract;

interface Parameters extends ArrayableContract, ToQueryContract
{
    const DELIMITER_DIVIDER = ',';
    const DELIMITER_PARAM = '_';
    const DELIMITER_PARAM_TO_QUERY = '.';
    const EXCLUDE_KEYS = ['fields', 'page', 'limit'];

    public function __construct(Collection $items = null);

    public function createByRequest(Request $request);

    public function addItem(ParameterContract $item);

    public function getItems(): Collection;

    public function setItems(Collection $items);
}
