<?php

namespace Sm\Contracts\Helpers\QueryFilters;

use Symfony\Component\HttpFoundation\ParameterBag;

interface ToQuery
{
    public function toQuery(): ParameterBag;
}
