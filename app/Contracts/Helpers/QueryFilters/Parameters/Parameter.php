<?php

namespace Sm\Contracts\Helpers\QueryFilters;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;

interface Parameter extends ArrayableContract
{

}
