<?php

namespace Sm\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    protected $dontReport = [];

    /**
     * @throws Exception
     */
    public function report(Exception $exc)
    {
        parent::report($exc);
    }

    public function render($request, Exception $exc): Response
    {
        if (env('APP_DEBUG', config('app.debug', false))) {
            return parent::render($request, $exc);
        }

        $apiResponseError = null;

        if ($exc instanceof ValidationException) {
            $apiResponseError = api_response_error($exc->getMessage(), $exc->errors(), $exc->status);
        }

        if ($exc instanceof NotFoundHttpException) {
            $apiResponseError = api_response_error(__("Not Found"), [], Response::HTTP_NOT_FOUND);
        }

        if ($exc instanceof UnauthorizedException) {
            $apiResponseError = api_response_error(__("Unauthorized"), [], Response::HTTP_UNAUTHORIZED);
        }

        if ($exc instanceof AuthorizationException) {
            $apiResponseError = api_response_error(
                __("You do not have permission to perform this action."),
                [],
                Response::HTTP_UNAUTHORIZED
            );
        }

        if ($exc instanceof ApiException) {
            $apiResponseError = api_response_error($exc->getMessage(), $exc->getContext(), $exc->getStatusCode());
        }

        if (!$apiResponseError && $exc instanceof Exception) {
            $apiResponseError = api_response_error($exc->getMessage());
        }

        return response($apiResponseError->toArray(), $apiResponseError->getStatusCode());
    }
}
