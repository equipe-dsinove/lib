<?php

namespace Sm\Exceptions;

use Throwable;

class NotFoundEntityException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?: __('Not Found Entity'), $code, $previous);
    }
}
