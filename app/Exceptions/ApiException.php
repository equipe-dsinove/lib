<?php

namespace Sm\Exceptions;

use Illuminate\Http\Response;
use Throwable;

class ApiException extends \Exception
{
    private $context;

    public function __construct(
        $message,
        array $context = [],
        $statusCode = Response::HTTP_BAD_REQUEST,
        Throwable $previous = null
    ) {
        parent::__construct($message, $statusCode, $previous);

        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }
}
