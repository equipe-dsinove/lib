<?php

namespace Sm\Traits;

use Carbon\CarbonInterface as CarbonContract;
use Illuminate\Database\Eloquent\Model;
use Sm\Contracts\Entities\Timestampsable as TimestampsableContract;

trait Timestamps
{
    private $createdAt;
    private $updatedAt;

    public function getCreatedAt(): ?CarbonContract
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?CarbonContract $createdAt): TimestampsableContract
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): ?CarbonContract
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?CarbonContract $updatedAt): TimestampsableContract
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    private function carbonToDate(): array
    {
        $data = [];

        if ($this->getCreatedAt()) {
            $data[Model::CREATED_AT] = $this->getCreatedAt()->format(TimestampsableContract::FORMAT_DATE_TIME);
        }

        if ($this->getUpdatedAt()) {
            $data[Model::UPDATED_AT] = $this->getUpdatedAt()->format(TimestampsableContract::FORMAT_DATE_TIME);
        }

        return $data;
    }

    public function humanizeTimestamps(): array
    {
        return $this->carbonToDate();
    }
}
