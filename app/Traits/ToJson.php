<?php

namespace Sm\Traits;

trait ToJson
{
    use ToObject;

    public function toJson($options = 0): string
    {
        return json_encode($this->toObject(), $options);
    }
}
