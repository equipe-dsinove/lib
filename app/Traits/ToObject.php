<?php

namespace Sm\Traits;

trait ToObject
{
    use ToArray;

    public function toObject(): \stdClass
    {
        return json_decode(json_encode($this->toArray()));
    }
}
