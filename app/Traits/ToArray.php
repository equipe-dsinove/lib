<?php

namespace Sm\Traits;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Sm\Contracts\Entity as EntityContract;

trait ToArray
{
    public function toArray(?bool $excludeEmpty = null): array
    {
        $data = [];

        $getters = array_filter(get_class_methods($this), function ($method) {
            return 'get' === substr($method, 0, 3);
        });

        foreach ($getters as $method) {
            $key = lcfirst(str_replace('get', '', $method));
            $value = $this->$method();

            if ($value instanceof EntityContract) {
                $value = $value->toArray($excludeEmpty);
            }

            if ($value instanceof ArrayableContract) {
                $value = $value->toArray();
            }

            if (is_object($value)) {
                $value = null;
            }
            $key = camelCaseToUnderScore($key);

            if ($excludeEmpty && $value) {
                $data[$key] = $value;
            }

            if (!$excludeEmpty) {
                $data[$key] = $value;
            }
        }

        return $data;
    }
}
