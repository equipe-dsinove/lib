<?php

namespace Sm\Policies;

use Illuminate\Support\Facades\Gate;
use \Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Sm\Contracts\Entity as ContractEntity;
use Sm\Contracts\Policies\PolicyApi as PolicyApiContract;
use Sm\Helpers\Pagination\Pagination;

class PolicyApiFactory
{
    public static function create(PolicyApiContract $policy): GateContract
    {
        $apiFind = function ($user, ContractEntity $entity) use ($policy) {
            return $policy->apiFind($user, $entity);
        };
        $apiFindBy = function ($user, Pagination $pagination) use ($policy) {
            return $policy->apiFindBy($user, $pagination);
        };
        $apiCreate = function ($user, ContractEntity $entity) use ($policy) {
            return $policy->apiCreate($user, $entity);
        };
        $apiUpdate = function ($user, ContractEntity $entity) use ($policy) {
            return $policy->apiUpdate($user, $entity);
        };
        $apiDelete = function ($user, ContractEntity $entity) use ($policy) {
            return $policy->apiDelete($user, $entity);
        };

        return Gate::define($policy->abilityName(PolicyApiContract::API_FIND), $apiFind)
            ->define($policy->abilityName(PolicyApiContract::API_FIND_BY), $apiFindBy)
            ->define($policy->abilityName(PolicyApiContract::API_CREATE), $apiCreate)
            ->define($policy->abilityName(PolicyApiContract::API_UPDATE), $apiUpdate)
            ->define($policy->abilityName(PolicyApiContract::API_DELETE), $apiDelete);
    }

    public static function createAuthorize(PolicyApiContract $policyApi, string $ability, $data)
    {
        if ($policyApi) {
            Gate::authorize($policyApi->abilityName($ability), $data);
        }
    }
}
