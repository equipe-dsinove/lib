<?php

namespace Sm\Policies;

use Illuminate\Support\Collection;
use Sm\Contracts\Entity as ContractEntity;
use Sm\Contracts\Models\ModelApi;
use Sm\Contracts\Policies\PolicyApi as PolicyApiContract;
use Sm\Entities\EntityFactory;
use Sm\Entities\User;
use Sm\Helpers\Pagination\Pagination;

abstract class AbstractPolicyApi implements PolicyApiContract
{
    protected function createUser($user): User
    {
        if ($user instanceof ModelApi) {
            $user = EntityFactory::createUser(collect($user->toArray()));
        }

        return $user;
    }

    public function after($user, $ability): bool
    {
        return $this->createUser($user)->isSuperAdmin();
    }

    public function apiFind($user, ContractEntity $entity): bool
    {
        return $this->createUser($user)->isAdmin();
    }

    public function apiFindBy($user, Pagination $pagination): bool
    {
        return $this->createUser($user)->isAdmin();
    }

    public function apiCreate($user, ContractEntity $entity): bool
    {
        return $this->createUser($user)->isAdmin();
    }

    public function apiUpdate($user, ContractEntity $entity): bool
    {
        return $this->createUser($user)->isAdmin();
    }

    public function apiDelete($user, ContractEntity $entity): bool
    {
        return $this->createUser($user)->isAdmin();
    }

    final public static function create(): PolicyApiContract
    {
        return new static();
    }
}
