<?php


if (!function_exists('config_path')) {
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('api_response')) {
    function api_response($data, int $statusCode = \Illuminate\Http\Response::HTTP_OK)
    {
        return \Sm\Helpers\ApiResponse::create($data, $statusCode);
    }
}

if (!function_exists('api_response_error')) {
    function api_response_error($data, array $context = [], int $statusCode = \Illuminate\Http\Response::HTTP_BAD_REQUEST)
    {
        return \Sm\Helpers\ApiResponseError::create($data, $context, $statusCode);
    }
}

if (!function_exists('camelCaseToUnderScore')) {
    function camelCaseToUnderScore(string $string): string
    {
        return strtolower(preg_replace(
            ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'],
            '$1_$2',
            $string
        ));
    }
}

if (!function_exists('dataToArray')) {
    function dataToArray($data)
    {
        if ($data instanceof \Illuminate\Contracts\Support\Arrayable) {
            $data = $data->toArray(true);
        } elseif (is_object($data)) {
            $data = (string)$data;
        }

        return $data;
    }
}
