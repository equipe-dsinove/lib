<?php

namespace Sm\Helpers\Pagination;

class Pagination
{
    const PAGE_DEFAULT = 1;
    const LIMIT_DEFAULT = 15;

    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
