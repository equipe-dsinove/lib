<?php

namespace Sm\Helpers\QueryFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;
use \Sm\Contracts\Helpers\QueryFilters\Parameters as ParametersContract;
use Symfony\Component\HttpFoundation\ParameterBag;

abstract class ParametersAbstract implements ParametersContract
{
    private $items;

    public function __construct(Collection $items = null)
    {
        $this->items = collect([]);

        if ($items) {
            $this->setItems($items);
        }
    }

    abstract public function createByRequest(Request $request);

    public function addItem(ParameterContract $item): self
    {
        $this->items->push($item);
        return $this;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function setItems(Collection $items): self
    {
        $items->each(function (ParameterContract $item, $key) {
            $this->addItem($item);
        });

        return $this;
    }

    abstract public function toArray(): array;

    abstract public function toQuery(): ParameterBag;
}
