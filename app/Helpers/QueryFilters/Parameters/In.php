<?php

namespace Sm\Helpers\QueryFilters\Parameters;

use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;

class In extends Condition implements ParameterContract
{
    public function __construct(string $field, $value)
    {
        parent::__construct($field, 'in', $value);
    }
}
