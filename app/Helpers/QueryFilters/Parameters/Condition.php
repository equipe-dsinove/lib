<?php

namespace Sm\Helpers\QueryFilters\Parameters;

use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;

class Condition extends Field implements ParameterContract
{
    const OPERATOR_EQUAL = 'eq';
    const OPERATOR_GREATER = 'gt';
    const OPERATOR_GREATER_EQUAL = 'gte';
    const OPERATOR_LESS = 'lt';
    const OPERATOR_LESS_EQUAL = 'lte';
    const OPERATOR_IN = 'in';
    const OPERATOR_NOT_IN = 'nin';
    const OPERATOR_DIFFERENT = 'neq';
    const OPERATOR_LIKE = 'like';
    const TYPE_DATETIME = 'datetime';
    const TYPE_YEAR = 'year';
    const TYPE_MONTH = 'month';
    const TYPE_DAY = 'day';
    const TYPE_HOUR = 'hour';
    const TYPE_MINUTE = 'minute';
    const TYPE_SECOND = 'second';

    const OPERATORS = [
        self::OPERATOR_EQUAL => '=',
        self::OPERATOR_GREATER => '>',
        self::OPERATOR_GREATER_EQUAL => '>=',
        self::OPERATOR_LESS => '<',
        self::OPERATOR_LESS_EQUAL => '<=',
        self::OPERATOR_DIFFERENT => '!=',
        self::OPERATOR_LIKE => 'LIKE',
        self::OPERATOR_IN => 'IN',
        self::OPERATOR_NOT_IN => 'NOT IN'
    ];

    const TYPES = [
        self::TYPE_DATETIME => 'DATE(:field)',
        self::TYPE_YEAR => 'YEAR(:field)',
        self::TYPE_MONTH => 'MONTH(:field)',
        self::TYPE_DAY => 'DAY(:field)',
        self::TYPE_HOUR => 'HOUR(:field)',
        self::TYPE_MINUTE => 'MINUTE(:field)',
        self::TYPE_SECOND => 'SECOND(:field)'
    ];


    private $operator;
    private $value;

    public function __construct(string $field, string $operator, $value)
    {
        parent::__construct($field);
        $this->operator = $operator;
        $this->value = $value;
    }

    public function getField(): string
    {
        return parent::getName();
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getValue()
    {
        return $this->value;
    }
}
