<?php

namespace Sm\Helpers\QueryFilters\Parameters;

use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;
use Sm\Contracts\Helpers\QueryFilters\ToQuery;
use Sm\Traits\ToArray;
use Symfony\Component\HttpFoundation\ParameterBag;

class Limit implements ParameterContract, ToQuery
{
    use ToArray;

    const LIMIT_NAME = 'limit';

    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function toQuery(): ParameterBag
    {
        return new ParameterBag([self::LIMIT_NAME => $this->getValue()]);
    }
}
