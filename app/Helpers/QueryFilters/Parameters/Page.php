<?php

namespace Sm\Helpers\QueryFilters\Parameters;

use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;
use Sm\Contracts\Helpers\QueryFilters\ToQuery;
use Symfony\Component\HttpFoundation\ParameterBag;

class Page extends Limit implements ParameterContract, ToQuery
{
    const PAGE_NAME = 'page';

    public function toQuery(): ParameterBag
    {
        return new ParameterBag([self::PAGE_NAME => $this->getValue()]);
    }
}
