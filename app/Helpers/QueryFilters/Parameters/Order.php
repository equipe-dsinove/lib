<?php

namespace Sm\Helpers\QueryFilters\Parameters;

use Sm\Contracts\Helpers\QueryFilters\Parameter as ParameterContract;
use Sm\Traits\ToArray;

class Order implements ParameterContract
{
    use ToArray;

    const ORDER_DESC = 'DESC';
    const ORDER_ASC = 'ASC';
    const ORDER_NAME = 'sort';

    private $field;
    private $direction;

    public function __construct(string $field, string $direction = self::ORDER_ASC)
    {
        $this->field = $field;
        $this->direction = $direction;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }
}
