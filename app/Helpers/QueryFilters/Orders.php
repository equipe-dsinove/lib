<?php

namespace Sm\Helpers\QueryFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sm\Helpers\QueryFilters\Parameters\Order;
use Sm\Traits\ToArray;
use Symfony\Component\HttpFoundation\ParameterBag;

class Orders extends ParametersAbstract
{
    use ToArray;
    const INVALID_ARGUMENT_DIRECTION = 'Direction order \':direction\' in query is not valid.';
    const SORT_NAME = 'sort';

    private function createOrder(ParameterBag $query, string $field, string $direction = Order::ORDER_ASC): self
    {
        $afterRemove = $field;

        $data = collect(explode(self::DELIMITER_PARAM, $field));
        $field = camelCaseToUnderScore($data->shift());

        if ($data->count() && $data->shift() !== self::SORT_NAME) {
            return $this;
        }

        $direction = strtoupper($direction);

        if (!in_array($direction, [Order::ORDER_ASC, Order::ORDER_DESC])) {
            throw new \InvalidArgumentException(__(self::INVALID_ARGUMENT_DIRECTION, [
                'direction' => $direction
            ]));
        }

        $this->addItem(new Order($field, $direction));

        $query->remove($afterRemove);

        return $this;
    }

    public function createByRequest(Request $request): self
    {
        foreach ($request->query->all() as $key => $item) {
            if (!in_array($key, self::EXCLUDE_KEYS)) {
                $this->createOrder($request->query, $key, $item);
            }
        }

        return $this;
    }

    public function toQuery(): ParameterBag
    {
        $data = new ParameterBag();

        $this->getItems()->each(function (Order $item, $key) use ($data) {
            $data->set(
                $item->getField() . self::DELIMITER_PARAM_TO_QUERY . self::SORT_NAME,
                strtolower($item->getDirection())
            );
        });

        return $data;
    }
}
