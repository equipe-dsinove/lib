<?php

namespace Sm\Helpers\QueryFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Sm\Helpers\QueryFilters\Parameters\Condition;
use Sm\Helpers\QueryFilters\Parameters\In;
use Sm\Helpers\QueryFilters\Parameters\NotIn;
use Sm\Helpers\QueryFilters\Parameters\Order;
use Symfony\Component\HttpFoundation\ParameterBag;

class Conditions extends ParametersAbstract
{
    const INVALID_ARGUMENT_TYPE = 'Type \':type\' in query is not valid.';
    const INVALID_ARGUMENT_OPERATOR = 'Operator \':operator\' in query is not valid.';

    private function addConditionInAndNotIn(string $field, string $operator, $value): void
    {
        $class = In::class;
        if ($operator === Condition::OPERATOR_NOT_IN) {
            $class = NotIn::class;
        }

        if (Str::contains($value, self::DELIMITER_DIVIDER)) {
            $value = explode(self::DELIMITER_DIVIDER, $value);
        }

        $this->addItem(new $class($field, $value));
    }

    private function createCondition(ParameterBag $query, string $field, $value): self
    {
        $afterRemove = $field;

        $data = collect(explode(self::DELIMITER_PARAM, $field));
        $field = camelCaseToUnderScore($data->shift());
        $operator = $data->count() ? $data->shift() : Condition::OPERATOR_EQUAL;

        if (in_array($operator, [Order::ORDER_NAME])) {
            return $this;
        }

        $like = $operator === Condition::OPERATOR_LIKE;

        if (!in_array($operator, array_keys(Condition::OPERATORS))) {
            throw new \InvalidArgumentException(__(self::INVALID_ARGUMENT_OPERATOR, [
                'operator' => $operator
            ]));
        }

        if ($data->count() && $type = $data->shift()) {
            if (!in_array($type, array_keys(Condition::TYPES))) {
                throw new \InvalidArgumentException(__(self::INVALID_ARGUMENT_TYPE, [
                    'type' => $type
                ]));
            }

            $field = strtr(Condition::TYPES[$type], [
                ':field' => $field
            ]);
        }

        if (Str::contains($operator, [Condition::OPERATOR_IN, Condition::OPERATOR_NOT_IN])) {
            $this->addConditionInAndNotIn($field, $operator, $value);
            return $this;
        }

        $operator = Condition::OPERATORS[$operator];

        if ($like) {
            $value = "%$value%";
        }

        $this->addItem(new Condition($field, $operator, $value));

        $query->remove($afterRemove);

        return $this;
    }

    public function createByRequest(Request $request): self
    {
        foreach ($request->query->all() as $key => $item) {
            if (!in_array($key, self::EXCLUDE_KEYS)) {
                $this->createCondition($request->query, $key, $item);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return $this->getItems()->map(function (Condition $condition) {
            return $condition->toArray();
        })->toArray();
    }

    public function toQuery(): ParameterBag
    {
        $data = new ParameterBag();

        $this->getItems()->each(function (Condition $item, $key) use ($data) {
            $delimiter = array_search(strtoupper($item->getOperator()), Condition::OPERATORS);
            $value = $item->getValue();

            if ($item instanceof In || $item instanceof NotIn) {
                $value = implode(self::DELIMITER_DIVIDER, $value);
            }

            $delimiter = self::DELIMITER_PARAM_TO_QUERY . $delimiter;

            if ($delimiter === self::DELIMITER_PARAM_TO_QUERY . Condition::OPERATOR_EQUAL) {
                $delimiter = '';
            }

            $data->set($item->getName() . $delimiter, $value);
        });

        return $data;
    }
}
