<?php

namespace Sm\Helpers\QueryFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sm\Contracts\Helpers\QueryFilters\ToQuery;
use Sm\Helpers\Pagination\Pagination;
use Sm\Helpers\QueryFilters\Parameters\Field;
use Sm\Helpers\QueryFilters\Parameters\Limit;
use Sm\Helpers\QueryFilters\Parameters\Page;
use Sm\Traits\ToArray;
use Symfony\Component\HttpFoundation\ParameterBag;

class BuildFilter
{
    use ToArray;

    private $query;
    private $fields;
    private $conditions;
    private $orders;
    private $limit;
    private $page;
    private $request;

    public function __construct(?Request $request = null)
    {
        $this->request = $request;
        $this->setup();
    }

    private function setup()
    {
        if (null === $this->request) {
            $this->setQuery(new ParameterBag())
                ->setFields((new Fields())->addItem(new Field('*')))
                ->setConditions(new Conditions())
                ->setOrders(new Orders())
                ->setLimit(Pagination::LIMIT_DEFAULT)
                ->setPage(Pagination::PAGE_DEFAULT);
        }

        if ($this->request) {
            $this->createQuery()->createFields()->createConditions()->createOrders()->createLimit()->createPage();
        }
    }

    public function createQuery(): self
    {
        $this->request->query->remove('q');
        return $this->setQuery($this->request->query);
    }

    public function getQuery(): ParameterBag
    {
        return $this->query;
    }

    public function setQuery(ParameterBag $query): self
    {
        $this->query = $query;
        return $this;
    }

    public function addQuery($key, $value): self
    {
        $this->getQuery()->set($key, $value);
        return $this;
    }

    private function createFields(): self
    {
        return $this->setFields((new Fields())->createByRequest($this->request));
    }

    public function getFields(): Fields
    {
        return $this->fields;
    }

    public function setFields(Fields $fields): self
    {
        $this->fields = $fields;
        return $this;
    }

    private function createOrders(): self
    {
        return $this->setOrders((new Orders())->createByRequest($this->request));
    }

    public function getOrders(): Orders
    {
        return $this->orders;
    }

    public function setOrders(Orders $orders): self
    {
        $this->orders = $orders;
        return $this;
    }

    private function createConditions(): self
    {
        return $this->setConditions((new Conditions())->createByRequest($this->request));
    }

    public function getConditions(): Conditions
    {
        return $this->conditions;
    }

    public function setConditions(Conditions $conditions): self
    {
        $this->conditions = $conditions;
        return $this;
    }

    private function createLimit(): self
    {
        $value = $this->request->query->get(Limit::LIMIT_NAME) ?: Pagination::LIMIT_DEFAULT;
        $this->request->query->remove(Limit::LIMIT_NAME);
        return $this->setLimit($value);
    }

    public function getLimit(): Limit
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = new Limit($limit);
        return $this;
    }

    private function createPage(): self
    {
        $value = $this->request->query->get(Page::PAGE_NAME) ?: Pagination::PAGE_DEFAULT;
        $this->request->query->remove(Page::PAGE_NAME);
        return $this->setPage($value);
    }

    public function getPage(): Page
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = new Page($page);
        return $this;
    }

    public function key(): string
    {
        return md5(json_encode($this->toArray()));
    }

    public function toQuery(): ParameterBag
    {
        $data = array_merge($this->getConditions()->toQuery()->all(), $this->getFields()->toQuery()->all());
        $data = array_merge($data, $this->getOrders()->toQuery()->all());
        $data = array_merge($data, $this->getPage()->toQuery()->all());
        $data = array_merge($data, $this->getLimit()->toQuery()->all());

        if (!$this->query) {
            $this->setQuery(new ParameterBag());
        }

        $this->getQuery()->add($data);

        return $this->getQuery();
    }
}
