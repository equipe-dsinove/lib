<?php

namespace Sm\Helpers\QueryFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Sm\Helpers\QueryFilters\Parameters\Field;
use Symfony\Component\HttpFoundation\ParameterBag;

class Fields extends ParametersAbstract
{
    const FIELDS_NAME = 'fields';

    private $items;

    public function createByRequest(Request $request): self
    {
        $items = collect([new Field('*')]);

        if ($data = $request->query->get(self::FIELDS_NAME)) {
            $items = collect(explode(self::DELIMITER_DIVIDER, $data))->map(function ($name) {
                return new Field($name);
            });
        }

        return $this->setItems($items);
    }

    public function toArray(): array
    {
        return $this->getItems()->map(function (Field $field) {
            return $field->toArray();
        })->toArray();
    }

    public function names(): array
    {
        return $this->getItems()->map(function (Field $field, $key) {
            return $field->getName();
        })->toArray();
    }

    public function toQuery(): ParameterBag
    {
        $names = $this->names();
        $data = !empty($names) ? [self::FIELDS_NAME => implode(self::DELIMITER_DIVIDER, $names)] : [];
        return new ParameterBag($data);
    }
}
