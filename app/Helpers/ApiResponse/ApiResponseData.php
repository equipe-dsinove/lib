<?php

namespace Sm\Helpers\ApiResponse;

use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface as ResponseInterfaceContract;

class ApiResponseData implements ArrayableContract
{
    private $response;

    public function __construct(ResponseInterfaceContract $response)
    {
        $this->response = $response;
    }

    public function getResponse(): ResponseInterfaceContract
    {
        return $this->response;
    }

    public function getBodyParse(): array
    {
        $string = (string)$this->response->getBody();
        return json_decode($string, true);
    }

    public function getStatus(): int
    {
        return $this->response->getStatusCode();
    }

    public function isSuccess(): bool
    {
        return in_array($this->getStatus(), [
            Response::HTTP_OK,
            Response::HTTP_CREATED,
            Response::HTTP_ACCEPTED,
            Response::HTTP_NO_CONTENT
        ]);
    }

    public function getData()
    {
        return $this->getBodyParse()['data'];
    }

    public function items(): array
    {
        return $this->getData();
    }

    public function toArray(): array
    {
        return $this->items();
    }

    public static function create(ResponseInterfaceContract $response)
    {
        return new static($response);
    }
}
