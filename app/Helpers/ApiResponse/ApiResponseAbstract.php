<?php

namespace Sm\Helpers;

use Illuminate\Http\Response;
use \Sm\Contracts\ApiResponse as ApiResponseContract;

abstract class ApiResponseAbstract implements ApiResponseContract
{
    private $data;
    private $statusCode;

    public function __construct($data, int $statusCode = Response::HTTP_OK)
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function toArray(): array
    {
        return [
            'status' => $this->getStatusCode(),
            'data' => dataToArray($this->getData())
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    public function __toString()
    {
        return $this->toJson();
    }
}
