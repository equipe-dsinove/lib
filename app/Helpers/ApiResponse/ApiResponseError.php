<?php

namespace Sm\Helpers;

use Illuminate\Http\Response;

class ApiResponseError extends ApiResponseAbstract
{
    private $context;

    public function __construct($data, array $context = [], int $statusCode = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($data, $statusCode);
        $this->context = $context;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function toArray(): array
    {
        $data = [
            'error' => [
                'code' => $this->getStatusCode(),
                'message' => $this->getData()
            ]
        ];

        if (!empty($this->getContext())) {
            $data['errors'] = $this->getContext();
        }

        return $data;
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    public function __toString()
    {
        return $this->toJson();
    }

    public static function create($data, array $context = [], int $statusCode = Response::HTTP_BAD_REQUEST)
    {
        return new static($data, $context, $statusCode);
    }
}
