<?php

namespace Sm\Helpers;

use Illuminate\Http\Response;

class ApiResponse extends ApiResponseAbstract
{
    public static function create($data, int $statusCode = Response::HTTP_OK): self
    {
        return new static($data, $statusCode);
    }
}
