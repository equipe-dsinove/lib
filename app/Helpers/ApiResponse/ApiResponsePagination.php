<?php

namespace Sm\Helpers;

use Illuminate\Http\Response;
use \Exception;
use Sm\Helpers\Pagination\Pagination;

class ApiResponsePagination extends ApiResponseAbstract
{
    public static function create($data, int $statusCode = Response::HTTP_OK): self
    {
        if (!$data instanceof Pagination) {
            throw new Exception(__('$data is not a ' . Pagination::class . ' class.'));
        }

        $data = array_map(function ($value) {
            if (is_object($value) && method_exists($value, 'toArray')) {
                $value = $value->toArray(true);
            } elseif (is_object($value)) {
                $value = (string)$value;
            }
            return $value;
        }, $data->getItems());

        return new static($data, $statusCode);
    }

    public function toArray(): array
    {
        return parent::toArray();
    }
}
