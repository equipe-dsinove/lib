<?php

namespace Sm\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\PromiseInterface;
use Sm\Helpers\ApiResponse\ApiResponseData;

class HttpService
{
    private $http;
    private $options = [
        'Content-Type' => 'application/json'
    ];

    public function __construct(string $baseUri, array $config = [], ?string $tokenBearer = null)
    {
        $config['base_uri'] = $baseUri;
        $this->http = new Client($config);
        $this->authBearer($tokenBearer);
    }

    private function authBearer(?string $tokenBearer): self
    {
        if ($tokenBearer) {
            $this->options['headers'] = [
                'Authorization' => "Bearer {$tokenBearer}"
            ];
        }

        return $this;
    }

    /**
     * @param string $uri
     * @param array $query
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function get(string $uri = '', array $query = [], array $options = []): ?ApiResponseData
    {
        $options['query'] = $query;

        return $this->send('GET', $uri, $options);
    }

    /**
     * @param string $uri
     * @param array $json
     * @param null $body
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function post(string $uri = '', array $json = [], $body = null, array $options = []): ?ApiResponseData
    {
        $options['json'] = $json;
        $options['body'] = $body;

        return $this->send('POST', $uri, $options);
    }

    /**
     * @param string $uri
     * @param array $multipart
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function postMultipart(string $uri = '', array $multipart = [], array $options = []): ?ApiResponseData
    {
        $options['multipart'] = $multipart;
        return $this->post($uri, [], null, $options);
    }

    /**
     * @param string $uri
     * @param array $formParams
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function postFormParams(string $uri = '', array $formParams = [], array $options = []): ?ApiResponseData
    {
        $options['form_params'] = $formParams;
        return $this->post($uri, [], null, $options);
    }

    /**
     * @param string $uri
     * @param array $json
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function put(string $uri = '', array $json = [], array $options = []): ?ApiResponseData
    {
        $options['json'] = $json;
        return $this->send('PUT', $uri, $options);
    }

    /**
     * @param string $uri
     * @param array $multipart
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function putMultipart(string $uri = '', array $multipart = [], array $options = []): ?ApiResponseData
    {
        $options['multipart'] = $multipart;
        return $this->put($uri, [], $options);
    }

    /**
     * @param string $uri
     * @param array $formParams
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function putFormParams(string $uri = '', array $formParams = [], array $options = []): ?ApiResponseData
    {
        $options['form_params'] = $formParams;
        return $this->put($uri, [], $options);
    }

    /**
     * @param string $uri
     * @param array $json
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function patch(string $uri = '', array $json = [], array $options = []): ?ApiResponseData
    {
        $options['json'] = $json;
        return $this->send('PATCH', $uri, $options);
    }

    /**
     * @param string $uri
     * @param array $multipart
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function patchMultipart(string $uri = '', array $multipart = [], array $options = []): ?ApiResponseData
    {
        $options['multipart'] = $multipart;
        return $this->patch($uri, [], $options);
    }

    /**
     * @param string $uri
     * @param array $formParams
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function patchFormParams(string $uri = '', array $formParams = [], array $options = []): ?ApiResponseData
    {
        $options['form_params'] = $formParams;
        return $this->patch($uri, [], $options);
    }

    /**
     * @param string $uri
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function delete(string $uri = '', array $options = []): ?ApiResponseData
    {
        return $this->send('DELETE', $uri, $options);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return ApiResponseData|null
     * @throws GuzzleException
     */
    public function send(string $method, string $uri = '', array $options = []): ?ApiResponseData
    {
        $options = array_merge_recursive($this->options, $options);

        return new ApiResponseData($this->http->request($method, $uri, $options));
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return PromiseInterface
     */
    public function sendAsync(string $method, string $uri = '', array $options = []): PromiseInterface
    {
        $options = array_merge_recursive($this->options, $options);

        return $this->http->requestAsync($method, $uri, $options);
    }

    public static function create(string $baseUri, array $config = [], ?string $tokenBearer = null): self
    {
        return new static($baseUri, $config, $tokenBearer);
    }
}
