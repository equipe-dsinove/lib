<?php

namespace Sm\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Contracts\Policies\PolicyApi as PolicyContract;
use Sm\Helpers\Pagination\Pagination;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Repositories\RepositoryApi as RepositoryContract;
use Sm\Contracts\Services\ServiceApi as ServiceApiContract;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;
use Sm\Policies\PolicyApiFactory;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractServiceApi implements ServiceApiContract
{
    private $repository;
    private $policy;
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $validationCustomAttributes = [];

    public function __construct(RepositoryContract $repository, PolicyContract $policy = null)
    {
        $this->repository = $repository;
        $this->policy = $policy;
    }

    public function getRepository(): RepositoryContract
    {
        return $this->repository;
    }

    public function getPolicy(): PolicyContract
    {
        return $this->policy;
    }

    /**
     * @return Model|ModelApiContract
     */
    public function getModel(): ModelApiContract
    {
        return $this->getRepository()->getModel();
    }

    public function apiFind(int $id, ?Fields $fields = null): ?EntityContract
    {
        if (!$entity = $this->getRepository()->apiFind($id, $fields)) {
            throw new NotFoundHttpException();
        }

        PolicyApiFactory::createAuthorize($this->getPolicy(), PolicyContract::API_FIND, $entity);

        return $entity;
    }

    public function apiFindBy(BuildFilter $build): Pagination
    {
        $pagination = $this->getRepository()->apiFindBy($build);

        PolicyApiFactory::createAuthorize($this->getPolicy(),PolicyContract::API_FIND_BY, $pagination);

        return $pagination;
    }

    public function validation(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        Validator::make($data, $rules, $messages, $customAttributes)->validate();
    }

    public function apiCreate(Request $request): ?EntityContract
    {
        $data = $request->all();

        $this->validation($data, $this->validationRules, $this->validationMessages, $this->validationCustomAttributes);

        $entity = $this->getRepository()->createEntity($data);

        PolicyApiFactory::createAuthorize($this->getPolicy(),PolicyContract::API_CREATE, $entity);

        if (!$this->getRepository()->apiCreate($entity)) {
            throw new \Exception(__('There was an error trying to create a new record.'));
        }

        return $entity;
    }

    public function apiUpdate(int $id, Request $request, bool $partial = null): ?EntityContract
    {
        $data = $request->all();

        $this->validation($data, $this->validationRules, $this->validationMessages, $this->validationCustomAttributes);

        $entity = $this->getRepository()->createEntity($data, $this->apiFind($id));

        PolicyApiFactory::createAuthorize($this->getPolicy(),PolicyContract::API_UPDATE, $entity);

        if (!$this->getRepository()->apiUpdate($id, $entity, $partial)) {
            throw new \Exception(__('There was an error trying to update a record.'));
        }

        return $entity;
    }

    public function apiUpdatePatch(int $id, Request $request): ?EntityContract
    {
        $data = $request->all();

        foreach ($this->validationRules as $key => $value) {
            if (!in_array($key, array_keys($data))) {
                unset($this->validationRules[$key]);
            }
        }

        return $this->apiUpdate($id, $request, true);
    }

    public function apiDelete(int $id): bool
    {
        if ($entity = $this->apiFind($id)) {
            PolicyApiFactory::createAuthorize($this->getPolicy(),PolicyContract::API_DELETE, $entity);
            return $this->getRepository()->apiDelete($id);
        }

        return false;
    }
}
