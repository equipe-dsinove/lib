<?php

namespace Sm\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as PaginatorContract;
use Illuminate\Contracts\Support\Arrayable as ArrayableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;
use Sm\Helpers\QueryFilters\Parameters\Condition;
use Sm\Helpers\QueryFilters\Parameters\In;
use Sm\Helpers\QueryFilters\Parameters\NotIn;
use Sm\Helpers\QueryFilters\Parameters\Order;

abstract class AbstractModelApi extends Model implements ModelApiContract
{
    use SoftDeletes;

    public function getKeyName(): string
    {
        return parent::getKeyName();
    }

    private function addConditions(BuildFilter $build, Builder &$query): self
    {
        $build->getConditions()->getItems()->each(function (Condition $condition) use ($query) {
            if ($condition instanceof In) {
                $query->whereIn($condition->getField(), $condition->getValue());
                return;
            }

            if ($condition instanceof NotIn) {
                $query->whereNotIn($condition->getField(), $condition->getValue());
                return;
            }

            $query->where($condition->getField(), $condition->getOperator(), $condition->getValue());
        });

        return $this;
    }

    public function addOrders(BuildFilter $build, Builder &$query): self
    {
        $build->getOrders()->getItems()->each(function (Order $order) use ($query) {
            $query->orderBy($order->getField(), $order->getDirection());
        });

        return $this;
    }

    /**
     * @param int $id
     * @param Fields|null $fields
     * @return ModelApiContract|Model|null
     */
    public function apiFind(int $id, ?Fields $fields = null): ?ModelApiContract
    {
        $fieldsArray = $fields ? array_values($fields->names()) : ['*'];

        return $this->query()->find($id, $fieldsArray);
    }

    public function apiFindBy(BuildFilter $build): PaginatorContract
    {
        $query = $this->query();
        $query->limit($build->getLimit()->getValue())->offset($build->getPage()->getValue());

        $fields = $build->getFields()->names();
        $fields = !empty($fields) ? $fields : ['*'];
        $this->addConditions($build, $query);
        $this->addOrders($build, $query);

        return $query->paginate(
            $build->getLimit()->getValue(),
            $fields,
            $build->getLimit()->getValue(),
            $build->getPage()->getValue()
        );
    }

    /**
     * @param EntityContract $entity
     * @return ArrayableContract|Model
     */
    public function apiCreate(EntityContract $entity): ModelApiContract
    {
        $entity->setCreatedAt(Carbon::now())->setUpdatedAt($entity->getCreatedAt());

        return $this->query()->create($entity->toArrayDb());
    }

    public function apiUpdate(int $id, EntityContract $entity, bool $partial = null): int
    {
        $entity->setUpdatedAt(Carbon::now());
        return $this->query()->where($this->getKeyName(), '=', $id)->update($entity->toArrayDb(true));
    }

    public function apiDelete(int $id): int
    {
        return $this->query()->where($this->getKeyName(), '=', $id)->delete();
    }
}
