<?php

namespace Sm\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Sm\Entities\EntityFactory;
use Sm\Entities\User;
use Sm\Helpers\Pagination\Pagination;
use \Exception;
use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\Models\ModelApi as ModelApiContract;
use Sm\Contracts\Repositories\RepositoryApi as RepositoryApiContract;
use Sm\Helpers\QueryFilters\BuildFilter;
use Sm\Helpers\QueryFilters\Fields;
use \Closure;

abstract class AbastractRepositoryApi implements RepositoryApiContract
{
    const BUILD_KEYS_ID = 'buildKeys';
    /**
     * Exemple: 'user:'.
     *
     * @var string
     */
    protected $keyMakeCache;

    /**
     * ttl in seconds
     *
     * @var int
     */
    protected $ttl = 60;

    protected $entityClassName;

    private $model;

    public function __construct(ModelApiContract $model)
    {
        $this->model = $model;
    }

    /**
     * @return Model|ModelApiContract
     */
    public function getModel(): ModelApiContract
    {
        return $this->model;
    }

    /**
     * @param array|null $data
     * @param EntityContract|null $entity
     * @return EntityContract|User
     * @throws \Sm\Exceptions\NotFoundEntityException
     */
    public function createEntity(?array $data, EntityContract $entity = null): ?EntityContract
    {
        if ($data && $data = collect($data)) {
            $data = EntityFactory::create($this->entityClassName, $data, $entity);
        }

        return $data;
    }

    private function keyFind(int $id, ?Fields $fields = null): string
    {
        $key = $id;

        if ($fields) {
            $key .= '-' . md5(json_encode($fields->toQuery()));
        }

        return $key;
    }

    /**
     * @param int $id
     * @param Fields|null $fields
     * @return null|EntityContract
     * @throws Exception
     */
    public function apiFind(int $id, ?Fields $fields = null)
    {
        $key = $this->keyFind($id, $fields);
        $this->buildKeys($this->makeKey($key));
        return $this->cache($key, function () use ($id, $fields) {
            if ($result = $this->getModel()->apiFind($id, $fields)) {
                return $result->entity();
            }

            return $result;
        });
    }

    /**
     * @param BuildFilter $build
     * @return Pagination
     * @throws Exception
     */
    public function apiFindBy(BuildFilter $build): Pagination
    {
        $key = $build->key();
        $this->buildKeys($this->makeKey($key));
        return $this->cache($key, function () use ($build) {
            $result = $this->getModel()->apiFindBy($build);
            $items = array_map(function (ModelApiContract $model) {
                return $model->entity();
            }, $result->items());

            return new Pagination($items);
        });
    }

    public function apiCreate(EntityContract &$entity): bool
    {
        $data = $this->getModel()->apiCreate($entity);

        if ($data) {
            $entity = $data->entity();
            return true;
        }

        return false;
    }

    /**
     * @param int $id
     * @param EntityContract $entity
     * @param bool|null $partial
     * @return bool
     * @throws Exception
     */
    public function apiUpdate(int $id, EntityContract &$entity, bool $partial = null): bool
    {
        if ((bool)$this->getModel()->apiUpdate($id, $entity, $partial)) {
            $this->removeCache($this->makeKey($id));
            $entity = $this->apiFind($id);
            return true;
        }

        return false;
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function apiDelete(int $id): bool
    {
        if ((bool)$this->getModel()->apiDelete($id)) {
            $this->removeCache($this->makeKey($id));
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return string
     * @throws Exception
     */
    protected function makeKey($id): string
    {
        if (empty($this->keyMakeCache)) {
            throw new Exception(__('$this->key not defined.'));
        }

        return $this->keyMakeCache . $id;
    }

    /**
     * @param null $buildKey
     * @param bool|null $remove
     * @return array
     * @throws Exception
     */
    public function buildKeys($buildKey = null, bool $remove = null): array
    {
        $keyId = $this->makeKey(self::BUILD_KEYS_ID);

        $keys = Cache::get($keyId, []);

        if ($buildKey && true !== $remove) {
            array_push($keys, $buildKey);
        }

        if ($remove) {
            foreach ($keys as $k => $key) {
                if ($key === $buildKey) {
                    unset($keys[$k]);
                    break;
                }
            }
        }

        if (null !== $buildKey) {
            Cache::put($keyId, $keys, $this->ttl);
        }

        return $keys;
    }

    /**
     * @param $key
     * @param Closure $callback
     * @return mixed
     * @throws Exception
     */
    protected function cache($key, \Closure $callback)
    {
        return Cache::remember($this->makeKey($key), $this->ttl, $callback);
    }

    /**
     * @param $key
     * @return bool
     * @throws Exception
     */
    protected function removeCache($key)
    {
        $keys = $this->buildKeys();
        array_push($keys, $key);

        foreach ($keys as $key) {
            $this->buildKeys($key, true);
            Cache::pull($key);
        }

        return true;
    }
}
