<?php

namespace Sm\Repositories;

use Sm\Entities\Project;

class ProjectRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'project:';
    protected $entityClassName = Project::class;
}
