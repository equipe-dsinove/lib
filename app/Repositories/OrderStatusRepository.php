<?php

namespace Sm\Repositories;

use Sm\Entities\OrderStatus;

class OrderStatusRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'order-status:';
    protected $entityClassName = OrderStatus::class;
}
