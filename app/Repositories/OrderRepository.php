<?php

namespace Sm\Repositories;

use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\OrderMethods as OrderMethodsContract;
use Sm\Entities\Order;

class OrderRepository extends AbastractRepositoryApi implements OrderMethodsContract
{
    protected $keyMakeCache = 'order:';
    protected $entityClassName = Order::class;

    public function status(EntityContract &$entity): bool
    {
        $data = $this->getModel()->status($entity);

        if ($data) {
            $entity = $data->entity();
            return true;
        }

        return false;
    }
}
