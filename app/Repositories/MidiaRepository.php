<?php

namespace Sm\Repositories;

use Sm\Entities\Midia;

class MidiaRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'midia:';
    protected $entityClassName = Midia::class;
}
