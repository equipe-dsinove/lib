<?php

namespace Sm\Repositories;

use Sm\Entities\Job;

class JobRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'job:';
    protected $entityClassName = Job::class;
}
