<?php

namespace Sm\Repositories;

use Sm\Entities\Template;

class TemplateRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'template:';
    protected $entityClassName = Template::class;
}
