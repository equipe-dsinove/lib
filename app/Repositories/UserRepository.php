<?php

namespace Sm\Repositories;

use Sm\Contracts\Entity as EntityContract;
use Sm\Contracts\UserMethods as UserMethodsContract;
use Sm\Entities\User;
use Sm\Helpers\QueryFilters\Fields;

class UserRepository extends AbastractRepositoryApi implements UserMethodsContract
{
    protected $keyMakeCache = 'user:';
    protected $entityClassName = User::class;

    /**
     * @param Fields|null $fields
     * @return EntityContract|User|null
     */
    public function me(?Fields $fields = null): ?EntityContract
    {
        $result = $this->getModel()->me($fields);

        if ($result) {
            return $result->entity();
        }

        return null;
    }
}
