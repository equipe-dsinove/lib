<?php

namespace Sm\Repositories;

use Sm\Contracts\Entity as EntityContract;
use Sm\Entities\TemplateOption;
use \Exception;

class TemplateOptionRepository extends AbastractRepositoryApi
{
    protected $keyMakeCache = 'template-option:';
    protected $entityClassName = TemplateOption::class;

    /**
     * @param EntityContract|TemplateOption $entity
     * @return bool
     * @throws Exception
     */
    public function apiCreate(EntityContract &$entity): bool
    {
        $this->removeCache($this->makeKey($entity->getTemplateId()));
        return parent::apiCreate($entity);
    }

    /**
     * @param int $id
     * @param EntityContract|TemplateOption $entity
     * @param bool|null $partial
     * @return bool
     * @throws Exception
     */
    public function apiUpdate(int $id, EntityContract &$entity, bool $partial = null): bool
    {
        $this->removeCache($this->makeKey($entity->getTemplateId()));
        return parent::apiUpdate($id, $entity, $partial);
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function apiDelete(int $id): bool
    {
        $entity = $this->apiFind($id);
        $this->removeCache($this->makeKey($entity->getTemplateId()));
        return parent::apiDelete($id);
    }
}
