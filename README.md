# Modelo de infraestrutura de microserviço - Lib

## Uso

---

### Desenvolvido com:
- [Lumen 6](https://lumen.laravel.com/docs/6.x)

### Dependências
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [PHP 7.3](https://www.php.net/)
- [Composer](https://getcomposer.org/)

## Desenvolvido e mantido por:
DS Inove Soluções – [david@dsinove.com.br](mailto:david@dsinove.com.br)
